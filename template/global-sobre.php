<?
$logo_array = ht_get_logo();

$logo_main = $logo_array['main']['url'];
?>

<div class="ht__sobre " style="background-image: url(<?php print  ht_get_theme_image("image/box1.png") ?>)">
    <div class="header">
        <h1>Conheça o</h1>
        <img src="<?php print $logo_main ?>" alt="logo">
        <p>Qualidade no atendimento e no amor de ser um restaurante familiar.</p>
    </div>
    <div class="body">
        <img src="<? print ht_get_theme_image("image/self-service.png") ?>" alt="self-service">
        <h1>Segunda a Sábado <br/> de <span>11h</span>  às <span>15h</span></h1>
        <p><span>KG</span>R$53 <span>,90</span></p>
    </div>
    <span class="line"></span>
    <div class="footer">
        <div>
            <div>
                <h1>Sábado coma <br/> <span>à vontade</span></h1>
            </div>
            <div>
                <p>Por <span>apenas</span></p>
                <h1>R$39<span>,90*</span></h1>
            </div>
        </div>
        <h1>*Não inclui comida japonesa</h1>
    </div>
    <a href="#pratos"><img src="<? print ht_get_theme_image("image/arrow-down.png") ?>" alt="arrow down"></a>
</div>