<?
    $args = array(  
        'post_type' => 'prato',
        'post_status' => 'publish',
        'posts_per_page' => 8, 
        'order' => 'ASC', 
    );

    $loop = new WP_Query( $args ); 
        
    $contact = ht_get_contact();
    $wppUrl = $contact["whatsapp"]['url'];
    $wppNumber = $contact["whatsapp"]['number'];
?>
<div class="ht__pratos" id="pratos" style="background-image: url(<?php print  ht_get_theme_image("image/box2.png") ?>)">
    <h1>Conheça também nossos</h1>
    <p>Pratos Executivos</p>
    <div class="pratos__carrossel-wrapper">
    
        <svg class="ht_prevArrow" width="30" height="83" viewBox="0 0 30 83" fill="none" xmlns="http://www.w3.org/2000/svg">
        <g clip-path="url(#clip0)">
        <path d="M30 22.0469L11.25 41.5L30 60.9531L26.25 68.7344L1.19045e-06 41.5L26.25 14.2656L30 22.0469Z" fill="white"/>
        </g>
        <defs>
        <clipPath id="clip0">
        <rect width="83" height="30" fill="white" transform="translate(0 83) rotate(-90)"/>
        </clipPath>
        </defs>
        </svg>

        <div class="pratos_carrossel">
        <? 
                while ( $loop->have_posts() ) : $loop->the_post(); 
        ?>
                <div class="prato">
                    <img src="<? print get_field("ht_prato-foto") ?>" alt="">
                    <h1><? print get_field("ht_prato-nome")?></h1>
                    <p>R$<? print get_field("ht_prato-valor")?></p>
                </div>
        <?
            endwhile;
        ?>
        </div>
        
        <svg class="ht_nextArrow" width="30" height="83" viewBox="0 0 30 83" fill="none" xmlns="http://www.w3.org/2000/svg">
        <g clip-path="url(#clip0)">
        <path d="M-2.04078e-06 22.0469L18.75 41.5L-3.40129e-07 60.9531L3.75 68.7344L30 41.5L3.75 14.2656L-2.04078e-06 22.0469Z" fill="white"/>
        </g>
        <defs>
        <clipPath id="clip0">
        <rect width="83" height="30" fill="white" transform="matrix(-4.37114e-08 -1 -1 4.37114e-08 30 83)"/>
        </clipPath>
        </defs>
        </svg>

    </div>
    <h1 class="disponibilidade">Prato do dia (consulte disponibilidade)</h1>
    <p class="disclaimer">*Todos os pratos acompanham arroz, feijão, batata frita e salada</p>
    <img src="<? print ht_get_theme_image("image/economia.png") ?>" alt="Economize tempo e ganhe sabor">
    <div class="promo">
        <img src="<? print ht_get_theme_image("image/promo 1.png")?>" alt="">
        <img src="<? print ht_get_theme_image("image/promo 2.png")?>" alt="">
    </div>
    <div class="pedido">
        <img  src="<? print ht_get_theme_image("image/prato.png")?>" alt="prato">
        <h1>Peça agora pelo Whatsapp</h1>
        <a href="<? print $wppUrl  ?>"><img src="<? print ht_get_theme_image("image/bi_whatsapp.png") ?>" alt="WhatsApp"><? print $wppNumber ?></a>
        <div>
            <h1>Entrega Grátris</h1>
            <p>no</p>
            <h1>Campos Elíseos</h1>
    </div>
    </div>
    <a href="#redes"><img src="<? print ht_get_theme_image("image/arrow-down.png") ?>" alt="arrow down"></a>

</div>