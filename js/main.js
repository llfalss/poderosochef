jQuery(document).ready(function($) {
  $('.pratos_carrossel').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    nextArrow: $('.ht_nextArrow'),
    prevArrow: $('.ht_prevArrow'),
    responsive: [
      {
        breakpoint: 1400,
        settings: {
          slidesToShow: 1,
        }
  }]});
});
